 **环境**
 
 php>=7.1

 swoole4.x

 

**安装** 

composer create-project  peter_yang/process_mm

 **配置** 

先复制一份配置
`cp config/config.php.example config/config.php`


```
    'cmd'=>[
         [
             'name'=>'laravel-queue',
             'bin'=>'/usr/local/php7.2/bin/php',//执行的命令
             'params'=>[
                 '/mnt/hgfs/USA/orderApi/artisan',//执行脚本路径，后面为参数
                 'queue:work',
                 '--queue=high,default,low',
                 '--tries=3',
                 '--timeout=30',
                 '--sleep=1'
             ],
             'num'=>3//开启进程数量
         ]
 
     ]

```        

进程挂掉后，主进程会重新拉起

 **启动**

``` 
php bin/start.php 
```

 **关闭**

``` 
php bin/stop.php 
```       

禁止某个进程重启拉起


```
php bin/disable_reload.php --name laravel-queue
```

