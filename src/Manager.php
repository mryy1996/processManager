<?php

namespace ProcessManager;

class Manager
{


    private $config = [];

    private $masterId = 0;

    private $processInfo = [];


    function __construct()
    {

        //获取主进程pid
        $this->masterId = posix_getpid();

        //读取配置项
        $this->config = include(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'config/config.php');


    }


    function run()
    {

        $this->saveMasterPid($this->masterId);

        foreach ($this->config['cmd'] as $key => $value) {


            $this->createProcess($value);


        }


    }

    function saveMasterPid($master_id){

        $binPath=dirname(__DIR__)."/bin/ProcessPid";

        file_put_contents($binPath,$master_id);

    }

    /**
     * 创建进程，执行命令
     * Create by Peter
     * 2019/10/09 10:44:22
     * Email:904801074@qq.com
     * @param $cmd
     */
    function createProcess($cmd)
    {

        for ($i = 0; $i < $cmd['num']; $i++) {


            $this->go($cmd);

        }
    }

    function go($cmd)
    {


        $process = new \swoole_process(function (\swoole_process $worker) use ($cmd) {


            $worker->exec($cmd['bin'], $cmd['params']);


        });

        $pid = $process->start();



        $this->processInfo[$pid] = $cmd;


        $this->saveProcessPid();
    }

    function saveProcessPid(){

        $binPath=dirname(__DIR__)."/bin/ProcessList";

        $processList=array_keys($this->processInfo);

        $processList=join(',',$processList);

        file_put_contents($binPath,$processList);
    }


    /**
     * 监听子进程退出
     * Create by Peter
     * 2019/10/09 10:44:16
     * Email:904801074@qq.com
     */
    function wait()
    {


        //要连续监听，不然有一个子进程崩溃后，主进程就退出了
        while (true) {

            $re = \swoole_process::wait();

            //异常退出，重新拉起
            $this->rebootProcess($re['pid']);

        }

    }

    /**
     * @return array|mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return int
     */
    public function getMasterId(): int
    {
        return $this->masterId;
    }

    /**
     * @return array
     */
    public function getProcessInfo(): array
    {
        return $this->processInfo;
    }


    /**
     * 重启子进程
     * Create by Peter
     * 2019/10/09 10:56:23
     * Email:904801074@qq.com
     * @param $pid
     */
    function rebootProcess($pid)
    {

        $info=$this->processInfo[$pid];

        $name=$info['name'];

        $disable_name=Web::$disable_reload->get($name)['name'];


        unset($this->processInfo[$pid]);


        print_r($disable_name);

        if($disable_name==$name) return;

        $this->go($info);


    }




}