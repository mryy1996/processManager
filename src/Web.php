<?php


namespace ProcessManager;


use Swoole\Http\Server;
use Swoole\Table;

class Web
{

    private $server;

    private $master_id;


    static public $disable_reload;

    function __construct()
    {

        //无法进程重启的队列表
        $disable_reload=new Table(1024);

        //进程名称
        $disable_reload->column('name',Table::TYPE_STRING,64);

        //操作时间
        $disable_reload->column('time',Table::TYPE_STRING,64);


        $disable_reload->create();

        self::$disable_reload=$disable_reload;


        $config = [
            'worker_num' => 1,
            'daemonize' => 1,
            'task_worker_num'=>1
        ];


        $server = new Server('0.0.0.0', 4575);

        $server->set($config);

        $server->on('start', [$this, 'start']);
//
        $server->on('WorkerStart', [$this, 'workerStart']);

        $server->on('Task', [$this, 'task']);

        $server->on('request', [$this, 'request']);

        $server->on('close', [$this, 'close']);

        $server->start();

        $this->server = $server;
    }

    function start(\swoole_http_server $server)
    {



        $this->master_id=$server->master_pid;

        //存储web的主pid进程
        $this->savePid($this->master_id);


    }

    function savePid($pid){

        $binPath=dirname(__DIR__)."/bin/WebPid";

        file_put_contents($binPath,$pid);

    }


    function workerStart(\swoole_server $server, int $worker_id){




        if($worker_id==0){

            $server->task('');



        }

    }

    function request(\swoole_http_request $request, \swoole_http_response $response)
    {



        $get=$request->get;


//        print_r($get);

        if(!$get) return;


        $type=$get['type'];


        switch ($type){

            case "disable_reload":

                $name=$get['name'];

                self::$disable_reload->set($name,['name'=>$name,'time'=>time()]);

                return $response->end('success');

                break;

            case "enable_reload":

                $name=$get['name'];


                self::$disable_reload->del($name);

                break;

        }


//
//        if($request->get){
//
//            print_r(self::$disable_reload->get($request->get['id']));
//
//        }else{
//            self::$disable_reload->set('qqq',['name'=>'qqq','time'=>time()]);
//        }



        $response->end('hello world!');


    }

    function close(\swoole_server $server, int $fd, int $reactorId)
    {


    }

    function processWork(){


        $process=new \swoole_process(function (\swoole_process $worker){





            $this->startProcess();

        });

        $process->start();


    }

    function task(\swoole_server $serv, int $task_id, int $src_worker_id,$data){


        $this->processWork();

    }


    function startProcess(){

        $manager=new Manager();

        $manager->run();

        $manager->wait();
    }

    /**
     * @return Table
     */
    public static function getDisableReload(): Table
    {
        return self::$disable_reload;
    }





}

